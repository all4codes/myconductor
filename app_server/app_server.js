
var Gearman = require("node-gearman");
var gearman_client = new Gearman("localhost", "4730");

io = require('socket.io').listen(7865).sockets.on('connection', function (socket)  {
    
    socket.on('conductorapp', function(data){
        console.log("dd");
        data.sid = socket.id;
        var job = gearman_client.submitJob("chat_listener", JSON.stringify(data), background = false);
        job.on("data", function(data){
            
            data = JSON.parse(data);
            console.log("data", data);
            if (data.type == "img_scan"){
                io.to(data.sid).emit("account_creation_event_status", data);
            }else if (data.type == "chat"){
                io.to(data.sid).emit("chat_response_event", data);
             }else if (data.type == "pin_scan"){
                io.to(data.sid).emit("account_login_event_status", data);
            }
        });
    });
    
    socket.on('disconnect', function() {
        
    });
    
    socket.on('fetch_record', function(data) {
        data.sid = socket.id;
        var job = gearman_client.submitJob("user_record", JSON.stringify(data), background = false);
        job.on("data", function(data){
            data = JSON.parse(data);
            console.log("data", data);
            io.to(data.sid).emit("user_record_event_status", data);
        });
    });
    
    
    socket.on('booking_record', function(data) {
        data.sid = socket.id;
         var job = gearman_client.submitJob("recorder", JSON.stringify(data), background = false);
        job.on("data", function(data){
            
            data = JSON.parse(data);
            console.log("data", data, data.sid);
            io.to(data.sid).emit("booking_record_event_status", data);
        });
    });
    
    
    
    socket.on('geo_location', function(data){
        console.log("dd");
        var job = gearman_client.submitJob("geo_location_python", JSON.stringify(data), background = false);
        job.on("data", function(data){
            console.log("data", data);
            socket.emit("chat_response_event", JSON.parse(data));
        });
    });

   
});



   

