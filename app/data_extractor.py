import json
import re
import redis
from pymongo import MongoClient
from app.get_class import TextClassifier
from gearman import GearmanClient, GearmanWorker
from app.journey_creater import JourneyCreater
import traceback
from copy import deepcopy

class DataExtraction(object):
    
    def __init__(self):
        self.client = MongoClient()
        self.journey_obj = JourneyCreater()
        # self.journey_obj.journeryListCreater()
        self.source_db = self.client["booking_data"]
        
        self.class_getter_obj = TextClassifier()
        self.priorit_question = {
            "1" : { "counter_text": ["Please provide me the details from source to destinaion. eg: From udaipur to jodhpur on 3/12/2017"], "key2check": ["source", "destination", "date"]},
            "2" : { "counter_text": ["Please provide me bus number. You can see it just behind chat box."], "key2check": ["bus_number"]},
            "3" : {"counter_text": ["Please give me your personal details in this given format eg. Pankaj kumar, 23, M, 12 | Aftab, 27, M, 45"], "key2check": ["persons"]}
        }
        self.ttl = 36000
        self.booked_text = "Your ticket has been booked. Please see it behind the chat box."
        self.redis_obj = redis.StrictRedis(host='localhost', port=6379, db=1)
        self.gm_client = GearmanClient(["127.0.0.1:4730"])
        self.gm_worker = GearmanWorker([ "127.0.0.1:4730"])

    def readRedis(self, uidai, sid):
        
        with open("./app/person_dict.json", "r") as f:
            redis_data = json.load(f)
            try:
                k = str(uidai)+str(sid)
                redis_keys = self.redis_obj.keys(pattern = k)
                if k not in redis_keys:
                    redis_data["uid"] = uidai
                    redis_data["sid"] = sid
                    self.redis_obj.setex(k, self.ttl, json.dumps(redis_data))
                else:
                    redis_data = json.loads(self.redis_obj.get(str(k)))
            except Exception, e:
                print "Error in getRedisData:\n", traceback.format_exc()
        return redis_data
    

    def extractBookingInfo(self, text_data, redis_data):
        sid = deepcopy(redis_data["sid"])
        uid = deepcopy(redis_data["uid"])
        return_text = ""
        text_data = re.sub(" +", " ", text_data)
        try:
            for each_person in text_data.split("|"):
                temp_dct = { "person_name": "", "age": "", "seat_no": "", "gender":""}    
                if len(each_person.split(",")) == 4:
                    temp_dct["person_name"] = (each_person.split(",")[0]).strip().title()
                    temp_dct["age"] = (each_person.split(",")[1]).strip()
                    temp_dct["gender"] = (each_person.split(",")[2]).strip()
                    temp_dct["seat_no"] = (each_person.split(",")[3]).strip()
                    redis_data["persons"].append(temp_dct)
            
            if redis_data["persons"]:
                bus_number = redis_data["bus_number"]
                all_trips = self.fetchDetailTrip(bus_number)
                source_collection = self.source_db["journey_list"]
            
                for t in all_trips:
                    if t["bus_number"] == bus_number:
                        for p in redis_data["persons"]:
                             t["filled_seats"].append(p["seat_no"])
                             t["filled_persons"].append(p)
                             source_collection.update({"bus_number" : bus_number}, t, upsert = True)
                return_text = self.booked_text
                
                
            
            persons_collection = self.source_db["persons_journey"]
            find = {"uid": uid, "sid": sid}
            insert = {"uid": str(uid), "sid": sid, "bus_number": bus_number, "booked" :redis_data["persons"] }
            persons_collection.update(find, insert, upsert = True)
            with open("./app/person_dict.json", "r") as f:
                    redis_data = json.load(f)
                    redis_data["sid"] = sid
                    redis_data["uid"] = uid
        except Exception as g:
            print g
        
        return return_text, redis_data

    def extractSourceDestination(self, text_data, redis_data):
        text_data = re.sub(" +", " ", text_data)
        if re.search(r'(from).*?(on)', text_data):
            text_data = re.search(r'(from).*?(on)', text_data).group()
            text_data = text_data.lstrip("from")
            text_data = text_data.rstrip("on")
            text_data = text_data.strip()

        source, destination = "", ""
        if len(text_data.split(" to ")) == 2:
            source = text_data.split(" to ")[0]
            destination = text_data.split(" to ")[1]
        redis_data["source"] = source.lower().strip()
        redis_data["destination"] = destination.lower().strip()
            
        return source, destination, redis_data
        
    def infoProvider(self, text_data, redis_data):
        text2ask = ""
        source, destination, redis_data = self.extractSourceDestination(text_data, redis_data)
        date = self.getDate(text_data)
        redis_data["source"] = source
        redis_data["destination"] = destination
        redis_data["date"] = date
        all_trips = self.fetchTrip(source, destination, date)
        #add in text all_trips
        
        
        return text2ask, redis_data, all_trips
        
    def extractBusNumber(self, text_data, redis_data):
        
        bus_number = ""
        regex_search = re.search(r"(RSRTC\d{4})", text_data, re.I)
        
        if regex_search:
            bus_number = regex_search.group()
        redis_data["bus_number"] = bus_number
        
        detail = self.fetchDetailTrip(bus_number)
        print detail
        # text2ask += detail
        text2ask = "You have selected "+ str(bus_number)+ ". "
        return text2ask, redis_data
    
    
    def asker(self, text_data, uidai, sid):
        flag = True
        text2ask = ""
        context = ""
        all_trips = []
        redis_data = self.readRedis(uidai, sid)
        
        text_data = re.sub("\!", "", text_data)
        user_data = {'query':text_data, 'topic':"unknown"}
        job_request = self.gm_client.submit_job("conductorapp_alice", json.dumps(user_data))
        if job_request and job_request.complete:
            text2ask = job_request.result
       
        if text_data and text2ask.strip() in ["0", 0, ""]:
            classifier_result = self.class_getter_obj.test(text_data)
            intent = classifier_result["class"]
            redis_data["intent"] = intent
            print "INTENT", intent
            all_trips = []
            if redis_data["intent"] == "booking":
                if redis_data["context"] in ["1", ""]:
                    text2ask, redis_data, all_trips = self.infoProvider(text_data, redis_data)
                    if all([True  if redis_data[key] else False for key in self.priorit_question["1"]["key2check"]]):
                        redis_data["context"] = "2"
                        text2ask += self.priorit_question["2"]["counter_text"][0]
                elif redis_data["context"] == "2":
                    text2ask, redis_data = self.extractBusNumber(text_data, redis_data)
                    if all([True if redis_data[key] else False for key in self.priorit_question["2"]["key2check"] ]):
                        redis_data["context"] = "3"
                        text2ask += self.priorit_question["3"]["counter_text"][0]
                elif redis_data["context"] == "3":
                    
                    text2ask, redis_data = self.extractBookingInfo(text_data, redis_data)
                
                    
            elif redis_data["intent"] == "available":
                text2ask, redis_data, all_trips = self.infoProvider(text_data, redis_data)

        if text2ask not in ["0", 0, ""]: flag = False
        
        while flag:
            print "For loop"
            priorities = self.priorit_question.keys()
            priorities.sort()
            
            for priority in priorities:
                flag_list = [True for key in self.priorit_question[priority]["key2check"] if redis_data[key]]
                if not flag_list or not all([True for key in self.priorit_question[priority]["key2check"] if redis_data[key]] ):
                    flag = False
                    text2ask = self.priorit_question[priority]["counter_text"][0]
                    redis_data["context"] = priority
                    break
        flag = True
        print "Redis Data: ", json.dumps(redis_data, indent = 4)
        self.redis_obj.setex(str(uidai)+str(sid), self.ttl, json.dumps(redis_data))
        return text2ask, all_trips, context 



    def fetchDetailTrip(self, bus_number):
        source_collection = self.source_db["journey_list"]
        trips = source_collection.find({"bus_number":bus_number})
        all_trips = []
        for trip in trips:
            del trip["_id"]
            all_trips.append(trip)
        return all_trips
    
    def bookBus(self):
        source_collection = self.source_db["journey_list"]
        source_collection.update({"source" : source, "destination": destination}, response_json, upsert = True)
    
    def fetchTrip(self, source, destination, date):
        source_collection = self.source_db["journey_list"]
        trips = source_collection.find({"source": source, "destination": destination, "date": date})
        all_possible_trip = []
        for trip in trips:
            del trip["_id"]
            all_possible_trip.append(trip)
            print trip
            
        return all_possible_trip

    def getDate(self, text_data):
        date = ""
        re_patterns = {
                        "date" : [r"(\d{1}|\d{2})[/.-](\d{2})[/.-](\d{4})"],
                    }

        for param in re_patterns.keys():
            for pattern in re_patterns[param]:
                search_obj = re.search(pattern, text_data)
                if search_obj:
                    date = search_obj.group(0)
        print date
        return date
        
if __name__ == '__main__':
    obj = DataExtraction()
    
    # flag = True
    # while flag:
        # print obj.asker(raw_input("Q: "), "12321")
        # print "\n\n"
        # print obj.extractSourceDestination(raw_input("Q: "))
        # print obj.getDate(raw_input("Q: "))
    # obj.extractBookingInfo("")