import re
import os
import sys
import glob
import json
import string
import gearman
import numpy as np
import nltk
from sklearn.multiclass import OneVsRestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from many_stop_words import get_stop_words
from nltk.tokenize import wordpunct_tokenize

class TextClassifier():
    def __init__(self):
        self.vectorizer = TfidfVectorizer( max_features = None, strip_accents = 'unicode',
                            analyzer = "word", ngram_range=(1,1), use_idf = 1, smooth_idf = 1, tokenizer=self.tokenizeText, stop_words='english')
        self.data_dict = {
            "available.txt": "available",
            "booking.txt":"booking"
        }
        self.class_labels = self.data_dict.keys()
        self.train()
        
    def tokenizeText(self, text):
        text = text.lower()
        tokens = []
        sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
        stop_words = set(get_stop_words('en'))
        stop_words_punct = [s for s in string.punctuation]
        text = re.sub(r'\.',' ', text)
        text = re.sub(r'\s+',' ', text)
        sents = sent_detector.tokenize(text.strip())
        # print sents[0:1]
        for stext in sents[0:1]:
            # tokens += filter(lambda x: x not in stop_words, wordpunct_tokenize(stext))
            tokens += wordpunct_tokenize(stext)
        return tokens
    
    def getTrainData(self,):
        xdata = []
        ydata = []
        train_path = "./app/"
        try:
            for ifile in self.class_labels:
                with open(train_path+ifile,"r") as tf:
                    tmpdata = tf.readlines()
                tmpdata = [ i.replace("\n","") for i in tmpdata ]
                tmpdata = [ i for i in tmpdata if i != "" ]
                for i in tmpdata:
                    xdata.append(i)
                    ydata.append(self.class_labels.index(ifile))
        except:
            pass
        
        return xdata, ydata
        

    def train(self):
        Xtrain, Ytrain = self.getTrainData()
        vectors_train = self.vectorizer.fit_transform(Xtrain)
        self.classifier = OneVsRestClassifier( LogisticRegression( C = 10.0, multi_class = "multinomial", solver = "lbfgs" ) )
        self.classifier.fit(vectors_train, Ytrain)

    
    def test(self, query):
        
        classifier_result = {}
        vectors_test = self.vectorizer.transform([query])
        pred = self.classifier.predict(vectors_test)
        predp = self.classifier.predict_proba(vectors_test)
        predp = predp.tolist()[0]
        max_ind =  predp.index(max(predp))
        classifier_result["class"] = self.data_dict[self.class_labels[max_ind]]
        classifier_result["prediction"] = {self.class_labels[max_ind]: round(max(predp)*100.0, 2)*0.6}
        return classifier_result


if __name__ == '__main__':
        obj = TextClassifier()
        while(True):
            qr = raw_input("=>")
            result = obj.test("book my seat")
            print result