from pymongo import MongoClient
import re
import json

class JourneyCreater(object):
    
    def __init__(self):
        self.client = MongoClient()
        self.source_db = self.client["booking_data"]
    
    def journeryListCreater(self):
        try:
            with open("./app/journey_list.txt", "r") as fp:
                journey_list = fp.readlines()
                source_collection = self.source_db["journey_list"]
                i = 0
                for each in journey_list[1:]:
                    i+=1
                    each = re.sub(" +", " ", each.rstrip("\n"))
                    if len(each.split(",")) > 5:
                        source = each.split(",")[0]
                        destination = each.split(",")[1]
                        fare = each.split(",")[2]
                        date = each.split(",")[3]
                        time = each.split(",")[4]
                        journey_time =  each.split(",")[5]
                        type_bus =  each.split(",")[6]
                        response_json = {
                                            "bus_number":"rsrtc123"+str(i), "filled_seats":[], "filled_persons":[],
                                            "type_bus":type_bus,
                                            "source" : source, "destination": destination, "fare": fare,
                                            "date": date, "time": time, "journey_time":journey_time
                                         }
                        source_collection.update({"source" : source, "destination": destination}, response_json, upsert = True)
            print "Jorney list updated!"
        except Exception as f:
            print "Error in journey ", f
        
                
if __name__ == '__main__':
    obj = JourneyCreater()
    obj.journeryListCreater()