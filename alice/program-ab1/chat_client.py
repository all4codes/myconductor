import gearman

class HermesClient(object):
    
    def __init__(self):
        self.gm_client = gearman.GearmanClient(['localhost:4730'])
        
    def check_request_status(self, job_request):
        if job_request.complete:
            #print "Job %s finished!  Result: %s - %s" % (job_request.job.unique, job_request.state, job_request.result)
	    print
	    print "~" * 20
	    print job_request.result
	    print "~" * 20
        elif job_request.timed_out:
            print "Job %s timed out!" % job_request.unique
        elif job_request.state == JOB_UNKNOWN:
            print "Job %s connection failed!" % job_request.unique

    def run(self, query = ""):
        completed_job_request = self.gm_client.submit_job("booking_api_alice", query)
        self.check_request_status(completed_job_request)

if __name__ == '__main__':
    # Name = "SWANAND"
    # HermesClient().run("SETNAME1 "+Name)
    query = raw_input("Chat with me: ")
    HermesClient().run(query)
    

