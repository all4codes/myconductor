var http = require("http"),
send = require("send"),
url = require("url"),
_ = require("underscore");

var fs = require("fs");
var config = JSON.parse(fs.readFileSync('../web/config.json', 'utf8'));
var serverIp = config.serverip;
var serverPort = config.serverport;

var app = http.createServer(function(req,res){

    function error(err){
        res.statusCode = err.status || 500;
        res.setHeader('Content-Type', 'text/html');
        res.end("<span style='font: 15px Tahoma; color: red'>Error: </span><span'>Page not Found! <br>Click <a href='http://"+serverIp+":"+serverPort+"''>here</a> to go to Home Page...</span></span>");
    }
    
    function redirect(){
        res.statusCode = 301;
        res.setHeader('Location', req.url + '/');
        res.end('Redirecting to ' + req.url + '/');
    }
    
    function setRoot(){
        res.setHeader("Access-Control-Allow-Origin", "*");
        console.log("http root")
        return '../web/';
    }
    
    function setIndex(){
        ip = req.connection.remoteAddress;
        res.setHeader("Access-Control-Allow-Origin", ip);
        return '../web/index.html';
    }
    
    
    send(req, url.parse(req.url).pathname, {root: setRoot(), index: setIndex(),extensions:['html', 'htm']})
	    .on('error', error)
	    .on('directory', redirect)
	    .pipe(res);
        
    
}).listen(serverPort);
