var domain = "localhost";
var serverUrl = "http://" + domain + ":" + 7865;
var socket = io.connect(serverUrl);

//bit.ly/rgh3-submit
function sendData(file) {
  //console.log("file", file  );
  socket.emit('conductorapp', {"img_url": file, "text_data": "", "type": "img_scan", "sid": "", "uid":""});
}

jQuery( document ).ready(function() {

  $('#send_pin').click(function() {
          var pin =  $("#pin").val();
          $("#output_pin").html("");
          $("#output_pin").append("<img src='loader.gif'/ style='width:150px;height:150px;'>");
          socket.emit('conductorapp', {"img_url": "", "text_data": pin, "type": "pin_scan", "sid": "", "uid":""});
  });

});

socket.on("account_login_event_status", function(res){
  $("#output_pin").html("");
  if (res.flag && res.details.uid !== undefined){
      var login_success = "<a> Login successful! <br> Conductor is being ready to assist you.</a>";
      $("#output_pin").append(login_success);
      setTimeout(function() {
        var param = window.btoa(res.details.uid+"_true");
        param = param.replace("=", "\=");
        console.log("param", param);
        window.location = "http://"+domain+":7864/index.html?param="+param;
      }, 4000);   
      
  }else{
      var login_failure = "<a> Login Failed! <br> Please try to login with valid pin.</a>";
      $("#output_pin").append(login_failure);
  }

});


socket.on("account_creation_event_status", function(res){
  $("#output").html("");
  console.log("account_creation_event_status", res);
  if (res.flag){
    var name_html = "";
    if (res.details.name !== undefined){
        name_html = "Hey "+ res.details.name+". ";
      }
      var success_html = "<a style='color:green;'>"+name_html+"<br>Registration is successfully completed! <br>Login Pin: "+res.pin+"</a>";
      $("#output").append(success_html);
  }
  else if(!res.flag ){
      var fail_html = "<a style='color:red;'>Registration Failed!<br> Please try with another QR code. </a>";
      $("#output").append(fail_html);
  }
  console.log("account_creation_event", res);
});

