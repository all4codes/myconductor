var domain = "localhost";
var serverUrl = "http://" + domain + ":" + "7865";
var transfer = "http://"+domain+":7864/scanner.html";

var socket = io.connect(serverUrl);
var remote_ip = '';
var req = new XMLHttpRequest();
var uid = "";

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    url = url.replace("\=", "=");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function validateLogin(){
    var str = getParameterByName("param", "");
    console.log("str", str);
    var enc = window.atob(str);
    var res = enc.split("_");
    uid = res[0];
    console.log("sdf", res);
    if (res.length == 2 && res[1]=="true"){
      console.log("success login");
      uid = res[0];
    }else{
      console.log("login failure");
      window.location = transfer;
    }
}


$(window).load(function () {
    validateLogin();
    socket.emit('booking_record', {"uid": uid, "sid": ""});
    socket.emit('fetch_record', {"uid": uid, "sid": ""});
    //GetGeo();
});
//setInterval(function(){ GetGeo(); }, 6000);

function GetGeo() {
  req.open('GET', document.location, false);
  req.send(null);
  var headers = req.getAllResponseHeaders().toLowerCase();
  var remote_ip = "default-user";
  if (headers.split("\n").length > 4) {
      console.log(headers);
      remote_ip = headers.split("\n")[4].replace("access-control-allow-origin: ", "").trim().split(":");
      remote_ip = remote_ip[remote_ip.length -1];
      console.log(remote_ip);
  }

  console.log("pp", remote_ip);
  var ip = remote_ip;
  $.getJSON("http://ip-api.com/json/" + ip, function (data) {
      console.log("pp", data);
      //socket.emit('geo_location', data);
    });
  
}
  
function openQRCamera(file) {
    console.log("rtrty", file);
    //var img = new Image();
    var reader = new FileReader();
    //var event = file;
    reader.onload = function(event) {
      var img = new Image();
      img.width = 300;
      img.src = event.target.result;
        console.log("rtrty", img.src);

        };
        reader.readAsDataURL(file);
}


function sendChat(){
  var text_data = $("#input_text").val();
  if (text_data.trim()!== ""){
    $("#input_text").val("");
    var html = '<div class="user-chat"><div class="well inner-user-chat"  style="border-radius:100px;"><img src="image/user.png" style="height:30px;width:30px;">&nbsp;&nbsp;&nbsp;' + text_data + '</div></div>';
    console.log("chat", text_data);
    socket.emit('conductorapp', {"text_data": text_data, "type": "chat", "sid": "", "uid":uid});
    $("#msgbox").append(html);
    var scrollDiv = $('#msgbox');
    scrollDiv.scrollTop(scrollDiv.prop("scrollHeight"));
  }

}


socket.on("chat_response_event", function(res){
  console.log("chat_response_event", res);
  
  if (res.trips_data.length>=1){
    for(var i in res.trips_data){
        if (i=== "0"){
    
          var bus_data = res.trips_data[i];
          var bus_html = `<div class="w3-cell-row">`+bus_data.bus_number+`
                            <div class="w3-cell" style="width:30%">
                              `+bus_data.source+` <===> `+ bus_data.destination+`<br>
                              `+bus_data.date+ `, `+bus_data.time+`<br>
                              Rs. `+bus_data.fare+`, `+bus_data.journey_time+`Hr.<br>
                            </div>
                            <div class="w3-cell w3-container">Total seats: 50<br>Occupied seat number: `+(bus_data.filled_seats).join(", ")+`
                            </div>
                          </div>  
                          <hr>`;
          $(".w3-container").html("");
          $(".w3-container").html("<h1>Available Seats</h1>");
          $(".w3-container").append(bus_html);
        }  
    }
  }else{
    socket.emit('booking_record', {"uid": uid, "sid": ""});
  }
  
    console.log("-=-==--=-=-=-=-=");
  
  html = '<div class="bot-chat"   ><div class="well inner-bot-chat"  style="border-radius:100px;"> <img src="image/bot.png" style="height:30px;width:30px;">&nbsp;&nbsp;&nbsp;' + res.text + '</div></div>';
  $("#msgbox").append(html);
    var scrollDiv = $('#msgbox');
    scrollDiv.scrollTop(scrollDiv.prop("scrollHeight"));

});

socket.on("account_creation_event", function(res){
  console.log("account_creation_event", res);
});

socket.on("user_record_event_status", function(res){
    var user = "";
    for (var k in res.user_data){
        user+= '<a class="w3-bar-item w3-button" href="#">'+k+": "+res.user_data[k]+'</a>';
    }
    $("#mySidebar").append(user);
  console.log("user_record_event_status", res);
});


socket.on("booking_record_event_status", function(res){
  console.log("booking_record_event_status", res);
  $(".w3-container").html("");
  $(".w3-container").html("<h1>Booked Seats By you.</h1>");
  var book_html = "";
  for (var each_dct_ind in res.recorded_data){
    console.log("ee",each_dct_ind);
    var each_ind = (res.recorded_data)[each_dct_ind];
    var p_html = "";
    
    for (var i in each_ind.booked){
        console.log("iiiiii", i);
        var inner = (each_ind.booked)[i];
        for (var k in inner){
            p_html += "<b>"+k+"</b>: "+ inner[k]+", ";
        }
        p_html="<p>"+p_html+"</p>";
    }
     book_html += `<div class="w3-cell-row">`+each_ind.bus_number+`
                            
                            <div class="w3-cell w3-container">`+p_html+`
                            </div>
                          </div>  
                          <hr>`;
    
    console.log(p_html, each_dct_ind);
    
  }
  $(".w3-container").append(book_html);
          
          
});


jQuery( document ).ready(function() {
    console.log("jquery");
    
    $(document).on('click','#chat_button', function(){
       $(".wrapper").toggle();
    });

    
    $('#input_text').keypress(function(event) {
        var keycode = event.keyCode || event.which;
        if(keycode == '13') {
            sendChat();
        }
    });
    $('.emoji').click(function() {
        sendChat();
    });



});
