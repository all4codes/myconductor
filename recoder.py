import json
import re
import redis
from pymongo import MongoClient
from gearman import GearmanClient, GearmanWorker
import traceback

class DataExtractionMongo(object):
    
    def __init__(self):
        self.client = MongoClient()
        self.source_db = self.client["booking_data"]
        self.gm_client = GearmanClient(["127.0.0.1:4730"])
        self.gm_worker = GearmanWorker([ "127.0.0.1:4730"])
        self.gm_worker.register_task("recorder", self.getBookedData )
        self.gm_worker.register_task("user_record", self.getUserData )

    def getUserData(self, gjob, gdata):
        
        data_dict = json.loads(gdata.data)
        res = { "sid": data_dict["sid"], "user_data":{}}
        uid = data_dict["uid"]
        source_collection = self.source_db["user_data"]
        for elem in source_collection.find({"uid": uid}):
            del elem["_id"]
            res["user_data"] = elem
        return json.dumps(res)
    
    
    def getBookedData(self, gjob, gdata):
        res = { "sid": "", "recorded_data":""}
        recorded_data = []
        data_dict = json.loads(gdata.data)
        uid = data_dict["uid"]
        sid = data_dict["sid"]
        source_collection = self.source_db["persons_journey"]
        try:
            for each in source_collection.find({"uid":uid}):
                del each["_id"]
                recorded_data.append(each)
        except Exception as f:
            print f
        print recorded_data
        res["recorded_data"] = recorded_data
        res["sid"] = sid
        
        return json.dumps(res)
        
        
if __name__ == '__main__':
    obj = DataExtractionMongo()
    print "recorder"
    obj.gm_worker.work()
    