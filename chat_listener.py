import json
import pdb
import sys
import gearman
import time
import traceback
import re
import os
from pyzbar.pyzbar import decode
from PIL import Image
from io import BytesIO
import base64
from pymongo import MongoClient
from random import randint
from app.data_extractor import DataExtraction

class ConvertData(object):
    def __init__(self):
        self.data_extraction_obj = DataExtraction()
        self.client = MongoClient()
        self.source_db = self.client["booking_data"]
        self.data_extraction_obj.journey_obj.journeryListCreater()
        self.gm_client = gearman.GearmanClient([ "localhost:4730" ])
        self.gm_worker = gearman.GearmanWorker([ "localhost:4730" ])
        self.gm_worker.register_task("chat_listener", self.main )

    def main(self, gjob, gdata):
        
        data_dict =  json.loads(gdata.data)
        context = ""
        response_data = { "text": "", "sid": data_dict["sid"], "type": "", "pin": "", "flag":"", "details": {}, "trips_data": [], "context": ""}
        try:
            query = data_dict["text_data"]
            query_type = data_dict["type"]
            sid = data_dict["sid"]
            uidai = data_dict["uid"]
            uid = uidai
            if query_type == "img_scan":
                pin, flag, final_dct = self.getUserData(data_dict)
                response_data["pin"] = pin
                response_data["flag"] = flag
                response_data["type"] = query_type
                response_data["details"] = final_dct
                
            elif query_type == "chat":
                response_data["text"], all_trips, context = self.data_extraction_obj.asker(query, uidai, sid)
                response_data["type"] = query_type
            elif query_type == "pin_scan":
                collection = self.source_db["user_data"]
                try:
                    
                    if len(query.split("@")) >1:
                        uid = query.split("@")[0]
                        pin = query.split("@")[1]
                        for k in collection.find({"uid": uid, "pin":pin}):
                            del k["_id"]
                            response_data["details"] = k
                except Exception as g:
                    print g
                response_data["type"] = query_type
                response_data["flag"] = True
            
            response_data["trips_data"] = all_trips
            response_data["context"] = context
            
            print "Response", json.dumps(response_data)
        except Exception:
            print 
        return json.dumps(response_data)

    def check_request_status(self, job_request):
        result = 0
        if job_request.complete:
            #print "Job %s finished!  Result: %s - %s" % (job_request.job.unique, job_request.state, job_request.result)
            print
            print "=" * 20
            print job_request.result
            print "=" * 20
            result = job_request.result
        elif job_request.timed_out:
            print "Job %s timed out!" % job_request.unique
        elif job_request.state == JOB_UNKNOWN:
            print "Job %s connection failed!" % job_request.unique
        return result
            
    def getUserData(self,data_dict):
        try:
            print "call"
            pin, flag = 0, False
            final_dct = {"uid":"",
                         "name":"",
                         "gender":"",
                         "yob":"", "co":"",
                         "loc":"",
                         "vtc":"",
                         "po":"",
                         "dist":"",
                         "subdist":"",
                         "state":"",
                         "pc":"",
                         "dob":""}
            base64data = data_dict["img_url"]
            base64data = re.sub("data\:image\/(png|jpg|jpeg);base64,","", base64data)
            img_obj = Image.open(BytesIO(base64.b64decode(base64data)))
            lst_obj = decode(img_obj)
            if lst_obj:
                for k in lst_obj[0]:
                    string = k
                    for key in final_dct:
                        searched = re.findall(''+key+'="([^"]*)"', string)
                        if searched:
                            final_dct[key] = searched[0]
                    break
            print final_dct
            
            collection = self.source_db["user_data"]
            if  final_dct["uid"]:
                flag = True
                pin = randint(1111,9999)
                final_dct["pin"] = str(pin)
                collection.update({"uid":final_dct["uid"]}, final_dct, upsert = True)
        except:
            print "Error in getUserData ", traceback.format_exc()

        return pin, flag, final_dct

if __name__ == '__main__':
    print "waiting for request:\n"
    cobj = ConvertData()
    cobj.gm_worker.work()
